using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdPersonMovement : MonoBehaviour
{
    public CharacterController controller;
    public float speed = 6f;

    void Awake()
    {
        controller = GetComponent<CharacterController>();
    }
    
    void Update()
    {
        float horizontalInput  = Input.GetAxisRaw("Horizontal");
        float VerticalInput  = Input.GetAxisRaw("Vertical");
        Vector3 direction = new Vector3(horizontalInput, 0f, VerticalInput).normalized;

        if (direction.magnitude >= 0.01f)
        {
            controller.Move(direction * speed * Time.deltaTime);
        }
    }
}
