using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace PLu
{
    public class NPCMover : MonoBehaviour
    {
        private NavMeshAgent _navMeshAgent;
        void Start()
        {
            Debug.LogFormat("NPCMover:Start");
            _navMeshAgent = GetComponent<NavMeshAgent>();
        }

        public void MoveTo(Vector3 position)
        {
            Debug.LogFormat("NPCMover:MoveTo navMeshAgent: {0}", _navMeshAgent);
            _navMeshAgent.destination = position;
        }
        public bool HasReachedDestination()
        {
            return _navMeshAgent.remainingDistance <= _navMeshAgent.stoppingDistance;
        }
    }
}
