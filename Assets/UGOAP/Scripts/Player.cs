using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PLu.Resources;

namespace PLu
{
    public class Player : MonoBehaviour
    {
        [SerializeField] private ResourceManager _resourceManager;
        private Inventory _inventory;

        void Start()
        {
            _inventory = GetComponent<Inventory>();
            Debug.Assert(_inventory != null, "Inventory component not found");    
        }
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                GetResource("IronOre");
            }
            
            if (Input.GetKeyDown(KeyCode.T))
            {
                GetResource("Wood");
            }

            if (Input.GetKeyDown(KeyCode.T))
            {
                StoreAll();
            }


            if (Input.GetKeyDown(KeyCode.L))
            {
                foreach (Item item in _inventory.GetItems())
                {
                    Debug.LogFormat("Player has {0} {1}", item.Quantity, item.name);
                }
            }
        }
        private void StoreAll()
        {
            foreach (Item item in _inventory.GetItems())
            {
                _inventory.RemoveQuantity(item.name, item.Quantity, out bool isExhausted);
                Debug.LogFormat("Player has {0} {1}", _inventory.QuantityOf(item.name), item.name);
                
            }
        }
        private void GetResource(string type)
        {
                ResourceSource resourceSource = _resourceManager.GetClosestResourceSource(type, transform.position);
                if (resourceSource != null)
                {
                    Item item = resourceSource.RemoveQuantity(1);
                    _inventory.AddItem(item);
                    Debug.LogFormat("Player has {0} {1}", _inventory.QuantityOf(type), type);
                }
                else
                {
                    Debug.LogFormat("No resource source for {0} is found", type);
                }
        }
    }
}
