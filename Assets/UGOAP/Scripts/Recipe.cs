using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
namespace PLu.Resources
{

    [CreateAssetMenu(fileName = "Recipe", menuName = "PLu/Recipe", order = 1)]
    public class Recipe : ScriptableObject
    {
        public string description;
        public List<Item> items;
        public CraftingStationType craftingStationType;
        public Item result;
    }
}
