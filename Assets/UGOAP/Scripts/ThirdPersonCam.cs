using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
namespace PLu
{
    public class ThirdPersonCam : MonoBehaviour
    {
        [Header("References")]
        public Transform orientation;
        public Transform player;
        public Transform playerObject;
        public Rigidbody rb;

        float rotationSpeed;

        void Start()
        {
            //Cursor.lockState = CursorLockMode.Locked;
            //Cursor.visible = false;
            rb = GameObject.Find("Player").GetComponent<Rigidbody>();
        }

        void Update()
        {
            Vector3 viewDirection = player.position - new Vector3(transform.position.x, player.position.y, transform.position.z);
            orientation.forward = viewDirection.normalized;

            float horizontalInput = Input.GetAxis("Horizontal");
            float verticalInput = Input.GetAxis("Vertical");
            Vector3 inputDirection = orientation.forward * verticalInput + orientation.right * horizontalInput;

            if (inputDirection != Vector3.zero)
            {
                playerObject.forward = Vector3.Slerp(playerObject.forward, inputDirection.normalized,  Time.deltaTime * rotationSpeed);
            }

        }

    }
}
