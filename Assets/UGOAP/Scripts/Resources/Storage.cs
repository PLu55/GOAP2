using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PLu.Resources
{   
    [RequireComponent(typeof(Inventory))]
    public class Storage : MonoBehaviour
    {
        private Inventory _inventory;

        void Start()
        {
            _inventory = GetComponent<Inventory>();
        } 
        public float QuantityOf(string type)
        {
            return _inventory.QuantityOf(type);
        }
        public void AddItem(Item item, int quantity)
        {
            _inventory.AddItem(item, quantity);
        }
        public int RemoveQuantity(string type, int quantity, out bool isExhausted)
        {
            return _inventory.RemoveQuantity(type, quantity, out isExhausted);
        }
    }
}
