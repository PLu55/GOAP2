using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
namespace PLu.Resources
{
    public class CraftingStation : MonoBehaviour
    {
        [SerializeField] CraftingStationType _type;

        public CraftingStationType type => _type;
        public string Name => _type.name;



    }
}
