using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
namespace PLu.Resources
{
    [CreateAssetMenu(fileName = "Item", menuName = "PLu/Item", order = 1)]
    public class Item : Resource
    {
        [SerializeField] private string _description;
        [SerializeField] private Sprite _icon;
        [SerializeField] private int _quantity;

        public string Description => _description;
        public Sprite Icon => _icon;
        public int Quantity => _quantity;

        public Item CreateItem(int quantity = -1)
        {
            Item newItem = CreateInstance<Item>();
            newItem.name = name;
            newItem._description = _description;
            newItem._icon = _icon;
            newItem._quantity = quantity == -1 ? _quantity : quantity;
            return newItem;
        }
        public Item Clone()
        {
            return CreateItem();
        }
        public void AddQuantity(int quantity)
        {
            this._quantity += quantity;
        }

        public int RemoveQuantity(int quantity, out bool exhausted)
        {
            exhausted = false;
            if (_quantity <= quantity)
            {
                quantity = this._quantity;
                exhausted = true;
            }
            this._quantity -= quantity;
            Debug.LogFormat("Removed {0} {1} remaning: {2}", quantity, name, this._quantity);
            return quantity;
        }
    }
}
