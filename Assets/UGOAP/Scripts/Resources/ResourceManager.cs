using System.Collections;
using System.Collections.Generic;
using UnityEditorInternal;
using UnityEngine;

namespace PLu.Resources
{
    public class ResourceManager : MonoBehaviour
    {
        private static ResourceManager _instance;
        public static ResourceManager Instance => _instance;

        [SerializeField] private List<string> ResourceTypes;
        private Dictionary<string, List<ResourceSource>> _resourceSources;
        private Dictionary<string, List<CraftingStation>> _craftingSations;
        
        private void Awake()
        {
            if (_instance == null)
            {
                _instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
            _resourceSources = new Dictionary<string, List<ResourceSource>>();
            _craftingSations = new Dictionary<string, List<CraftingStation>>();
            FindAllResources();        
        }
        void Start()
        {
            //_resources = new Dictionary<string, List<GameObject>>();
            //FindAllResouirces();
        }
        private void FindAllResources()
        {
            GameObject[] gameObjects = GameObject.FindGameObjectsWithTag("Resource");
            Debug.LogFormat("ResourceManager found {0} objects with the Resource tag", gameObjects.Length);
            foreach (GameObject gameObject in gameObjects)
            {
                var resourceSource = gameObject.GetComponent<ResourceSource>();
                if (resourceSource != null)
                {
                    Debug.LogFormat("ResourceManager found: {0} {1}", resourceSource, resourceSource.Name);
                    AddResource(resourceSource);
                    resourceSource.ResourceSourceExhausted += OnResourceExhausted;
                    continue;
                }
                if (gameObject.GetComponent<CraftingStation>() != null)
                {
                    var craftingStation = gameObject.GetComponent<CraftingStation>();
                    Debug.LogFormat("ResourceManager found: {0} {1}", craftingStation, craftingStation.Name);
                    AddCraftingStation(craftingStation);
                }
            }
        }
        public void AddCraftingStation(CraftingStation craftingStation)
        {
            if (!_craftingSations.ContainsKey(craftingStation.Name))
            {
                _craftingSations.Add(craftingStation.Name, new List<CraftingStation>());
            }
            _craftingSations[craftingStation.Name].Add(craftingStation);
        }
        public List<CraftingStation> GetCraftingStations(string type)
        {
            Debug.Assert(_craftingSations != null);
            if (_craftingSations.ContainsKey(type))
            {
                return _craftingSations[type];
            }
            Debug.LogFormat("No: {0} found!", type);
            return null;
        }
        public CraftingStation GetClosestCraftingStation(string type, Vector3 position)
        {
            List<CraftingStation> craftingStations = GetCraftingStations(type);
            if (craftingStations == null || craftingStations.Count == 0)
            {
                return null;
            }

            CraftingStation closest = null;
            float minDistance = float.MaxValue;

            foreach (var craftingStation in craftingStations)
            {
                Vector3 craftingStationPosition = craftingStation.GetComponent<Transform>().position;
                float sqrDistance = (position - craftingStationPosition).sqrMagnitude;
                if (sqrDistance < minDistance)
                {
                    closest = craftingStation;
                    minDistance = sqrDistance;
                }
            }
            return closest;
        }
        public void AddResource(ResourceSource resourceSource)
        {
            if (! _resourceSources.ContainsKey(resourceSource.Name))
            {
                _resourceSources.Add(resourceSource.Name, new List<ResourceSource>());
            }
            _resourceSources[resourceSource.Name].Add(resourceSource); 
        }
        public List<ResourceSource> GetResources(string type)
        {
            Debug.Assert(_resourceSources != null);
            if (_resourceSources.ContainsKey(type))
            {
                return _resourceSources[type];
            }
            Debug.LogFormat("No: {0} found!", type);
            return null;
        }
        public ResourceSource GetClosestResourceSource(string type, Vector3 position)
        {
            List<ResourceSource> resourceSources = GetResources(type);
            if (resourceSources == null || resourceSources.Count == 0)
            {
                return null;
            }

            ResourceSource closest = null;
            float minDistance = float.MaxValue;

            foreach (var resourceSource in resourceSources)
            {
                Vector3 resourceSourcePosition = resourceSource.GetComponent<Transform>().position;
                float sqrDistance = (position - resourceSourcePosition).sqrMagnitude;
                if (sqrDistance < minDistance)
                {
                    closest = resourceSource;
                    minDistance = sqrDistance;
                }
            }
            return closest;
        }
        private void OnResourceExhausted(ResourceSource resourceSource)
        {

            List<ResourceSource> resourceSources;
            if (_resourceSources.TryGetValue(resourceSource.Name, out resourceSources))
            {
                Debug.LogFormat("Removing {0} from ResourceManager", resourceSource);
                resourceSources.Remove(resourceSource);
            }
        }


    }
}
