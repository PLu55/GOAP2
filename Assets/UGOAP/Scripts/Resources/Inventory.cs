using System.Collections;
using System.Collections.Generic;
using Unity.Loading;
using Unity.VisualScripting;
using UnityEngine;

namespace PLu.Resources
{
    public class Inventory : MonoBehaviour
    {
        [SerializeField] private Dictionary<string, Item> _contents; 

        void Awake()
        {
            _contents = new Dictionary<string, Item>();
        }
        public int QuantityOf(string type)
        {
            if (!_contents.ContainsKey(type))
            {
                return 0;
            }

            return _contents[type].Quantity;
        }

        public void AddItem(Item item, int quantity = 1)
        {
            if (_contents.ContainsKey(item.name))
            {
                _contents[item.name].AddQuantity(item.Quantity);
            }
            else
            {
                _contents.Add(item.name, item.Clone());
            }
        }

        public int RemoveQuantity(string type, int quantity, out bool isExhausted)
        {
            isExhausted = false;
            if (!_contents.ContainsKey(type))
            {
                return 0;
            }

            quantity = _contents[type].RemoveQuantity(quantity, out isExhausted);
            return quantity;
        }

        public IEnumerable<Item> GetItems()
        {
            return _contents.Values;
        }
    }
}
