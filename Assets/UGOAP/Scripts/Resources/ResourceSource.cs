using System.Collections;
using System.Collections.Generic;
using UnityEditor.Build;
using UnityEngine;
using TMPro;
using Unity.VisualScripting;

namespace PLu.Resources
{
    public class ResourceSource : MonoBehaviour
    {
        [SerializeField] private Item _item;
        [SerializeField] private int _initialQuantity;

        [SerializeField] private TMP_Text _label;

        public string Name => _item.name;
        public Item Item => _item;
        public int Quantity => _item.Quantity;

        public delegate void ResourceSourceExhaustedEventHandler(ResourceSource resourceSource);
        public event ResourceSourceExhaustedEventHandler ResourceSourceExhausted;

        void OnEnable()
        {
            Debug.Log("ResourceSource.OnEnable");
        }
        void Awake()
        {
            _item = _item.CreateItem(_initialQuantity);
            _label.text = _item.name;
        }

        private void OnResourceSourceExhausted()
        {
            ResourceSourceExhausted?.Invoke(this);
        }
        public Item RemoveQuantity(int quantity)
        {
            bool isExhausted;
            quantity = _item.RemoveQuantity(quantity, out isExhausted);
            if (isExhausted)
            {
                OnResourceSourceExhausted();
                Destroy(gameObject);
            }
            
            return Item.CreateItem(quantity);;
        }

    }
}
