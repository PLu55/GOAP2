using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

using DependencyInjection;
using Unity.VisualScripting;
using PLu.Resources;


namespace PLu
{
    public class MyAgent : MonoBehaviour
    {
        [Header("Sensors")] 
        [SerializeField] Sensor chaseSensor;
        [SerializeField] Sensor attackSensor;
        [SerializeField] AnimationController animations;
        
        [Header("Known Locations")] 
        [SerializeField] public Transform restingPosition;
        [SerializeField] public Transform foodShack;
        [SerializeField] public Transform doorOnePosition;
        [SerializeField] public Transform doorTwoPosition;
        [SerializeField] public Transform storagePosition;
        
            
        [Header("Stats")] 
        public float health = 100;
        public float stamina = 100;

        CountdownTimer statsTimer;

        private IGoapAgent _goapAgent;
        private NavMeshAgent _navMeshAgent;
        private Inventory _inventory;
        void Awake() 
        {
            _navMeshAgent = GetComponent<NavMeshAgent>();   
            _goapAgent = GetComponent<GoapAgent>();
            _inventory = GetComponent<Inventory>();
        }

        void Start()
        {
            SetupBeliefs();
            SetupActions();
            SetupGoals();
            SetupTimers();
        }
        void Update()
        {
            statsTimer.Tick(Time.deltaTime);
        }
        private void SetupBeliefs() 
        {
            BeliefFactory factory = new BeliefFactory(_goapAgent, _goapAgent.Beliefs);
            
            factory.AddBelief("Nothing", () => false);
            
            factory.AddBelief("AgentIdle", () => !_navMeshAgent.hasPath);
            factory.AddBelief("AgentMoving", () => _navMeshAgent.hasPath);
            factory.AddBelief("AgentHealthLow", () => health < 30);
            factory.AddBelief("AgentIsHealthy", () => health >= 50);
            factory.AddBelief("AgentStaminaLow", () => stamina < 10);
            factory.AddBelief("AgentIsRested", () => stamina >= 50);
            
            factory.AddResourceBelief("AgentHasIronOre", _inventory, "IronOre", 5);
            
            factory.AddLocationBelief("AgentAtDoorOne", 2f, doorOnePosition);
            factory.AddLocationBelief("AgentAtDoorTwo", 2f, doorTwoPosition);
            factory.AddLocationBelief("AgentAtRestingPosition", 3f, restingPosition);
            factory.AddLocationBelief("AgentAtFoodShack", 3f, foodShack);
            factory.AddLocationBelief("AgentAtStorage", 3f, storagePosition);
            
            factory.AddSensorBelief("PlayerInChaseRange", chaseSensor);
            factory.AddSensorBelief("PlayerInAttackRange", attackSensor);
            factory.AddBelief("AttackingPlayer", () => false); // Player can always be attacked, this will never become true
        }

        private void SetupActions() 
        {            
            _goapAgent.Actions.Add(new AgentAction.Builder("Relax")
                .WithStrategy(new IdleStrategy(5))
                .AddEffect(_goapAgent.Beliefs["Nothing"])
                .Build());
            
            _goapAgent.Actions.Add(new AgentAction.Builder("Wander Around")
                .WithStrategy(new WanderStrategy(_navMeshAgent, 10))
                .AddEffect(_goapAgent.Beliefs["AgentMoving"])
                .Build());

            _goapAgent.Actions.Add(new AgentAction.Builder("MoveToEatingPosition")
                .WithStrategy(new MoveStrategy(_navMeshAgent , () => foodShack.position))
                .AddEffect(_goapAgent.Beliefs["AgentAtFoodShack"])
                .Build());
            
            _goapAgent.Actions.Add(new AgentAction.Builder("Eat")
                .WithStrategy(new IdleStrategy(5))  // Later replace with a Command
                .AddPrecondition(_goapAgent.Beliefs["AgentAtFoodShack"])
                .AddEffect(_goapAgent.Beliefs["AgentIsHealthy"])
                .Build());

            _goapAgent.Actions.Add(new AgentAction.Builder("MoveToDoorOne")
                .WithStrategy(new MoveStrategy(_navMeshAgent , () => doorOnePosition.position))
                .AddEffect(_goapAgent.Beliefs["AgentAtDoorOne"])
                .Build());

            _goapAgent.Actions.Add(new AgentAction.Builder("MoveToDoorTwo")
                .WithStrategy(new MoveStrategy(_navMeshAgent , () => doorTwoPosition.position))
                .AddEffect(_goapAgent.Beliefs["AgentAtDoorTwo"])
                .Build());

            _goapAgent.Actions.Add(new AgentAction.Builder("MoveFromDoorOneToRestArea")
                .WithCost(2)
                .WithStrategy(new MoveStrategy(_navMeshAgent , () => restingPosition.position))
                .AddPrecondition(_goapAgent.Beliefs["AgentAtDoorOne"])
                .AddEffect(_goapAgent.Beliefs["AgentAtRestingPosition"])
                .Build());

            _goapAgent.Actions.Add(new AgentAction.Builder("MoveFromDoorTwoRestArea")
                .WithStrategy(new MoveStrategy(_navMeshAgent , () => restingPosition.position))
                .AddPrecondition(_goapAgent.Beliefs["AgentAtDoorTwo"])
                .AddEffect(_goapAgent.Beliefs["AgentAtRestingPosition"])
                .Build());

            _goapAgent.Actions.Add(new AgentAction.Builder("Rest")
                .WithStrategy(new IdleStrategy(5))
                .AddPrecondition(_goapAgent.Beliefs["AgentAtRestingPosition"])
                .AddEffect(_goapAgent.Beliefs["AgentIsRested"])
                .Build());

            _goapAgent.Actions.Add(new AgentAction.Builder("ChasePlayer")
                .WithStrategy(new MoveStrategy(_navMeshAgent , () => _goapAgent.Beliefs["PlayerInChaseRange"].Location))
                .AddPrecondition(_goapAgent.Beliefs["PlayerInChaseRange"])
                .AddEffect(_goapAgent.Beliefs["PlayerInAttackRange"])
                .Build());

            _goapAgent.Actions.Add(new AgentAction.Builder("AttackPlayer")
                .WithStrategy(new AttackStrategy(animations))
                .AddPrecondition(_goapAgent.Beliefs["PlayerInAttackRange"])
                .AddEffect(_goapAgent.Beliefs["AttackingPlayer"])
                .Build());
        }

        private void SetupGoals() 
        {   
            _goapAgent.Goals.Add(new AgentGoal.Builder("Chill Out")
                .WithPriority(1)
                .WithDesiredEffect(_goapAgent.Beliefs["Nothing"])
                .Build());
            
            _goapAgent.Goals.Add(new AgentGoal.Builder("Wander")
                .WithPriority(1)
                .WithDesiredEffect(_goapAgent.Beliefs["AgentMoving"])
                .Build());
            
            _goapAgent.Goals.Add(new AgentGoal.Builder("KeepHealthUp")
                .WithPriority(2)
                .WithDesiredEffect(_goapAgent.Beliefs["AgentIsHealthy"])
                .Build());

            _goapAgent.Goals.Add(new AgentGoal.Builder("KeepStaminaUp")
                .WithPriority(2)
                .WithDesiredEffect(_goapAgent.Beliefs["AgentIsRested"])
                .Build());
            
            _goapAgent.Goals.Add(new AgentGoal.Builder("SeekAndDestroy")
                .WithPriority(3)
                .WithDesiredEffect(_goapAgent.Beliefs["AttackingPlayer"])
                .Build());
        }
            // TODO move to stats system
        void SetupTimers() 
        {
            statsTimer = new CountdownTimer(2f);
            statsTimer.OnTimerStop += () => {
                UpdateStats();
                statsTimer.Start();
            };
            statsTimer.Start();
        }

        // TODO move to stats system
        void UpdateStats() 
        {
            stamina += InRangeOf(restingPosition.position, 3f) ? 20 : -2;
            health += InRangeOf(foodShack.position, 3f) ? 20 : -1;
            stamina = Mathf.Clamp(stamina, 0, 100);
            health = Mathf.Clamp(health, 0, 100);
        }
        bool InRangeOf(Vector3 pos, float range) => Vector3.Distance(transform.position, pos) < range;
    
        void OnEnable() => chaseSensor.OnTargetChanged += _goapAgent.ForceReplanning;
        void OnDisable() => chaseSensor.OnTargetChanged -= _goapAgent.ForceReplanning;

    }
}
