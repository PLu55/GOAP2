using System.Collections.Generic;
using System.Linq;
using DependencyInjection; // https://github.com/adammyhre/Unity-Dependency-Injection-Lite
using PLu.Resources;
using UnityEngine;
using UnityEngine.AI;


public interface IGoapAgent
{
    Dictionary<string, AgentBelief> Beliefs { get; }
    HashSet<AgentAction> Actions { get; }
    HashSet<AgentGoal> Goals { get; }
    Transform Transform { get; }
    void ForceReplanning();
}

[RequireComponent(typeof(NavMeshAgent))]
//[RequireComponent(typeof(AnimationController))]
public class GoapAgent : MonoBehaviour,  IGoapAgent {

    NavMeshAgent navMeshAgent;
    AnimationController animations;
    Rigidbody rb;
    
    AgentGoal lastGoal;
    public AgentGoal currentGoal;
    public ActionPlan actionPlan;
    public AgentAction currentAction;
    
    private Dictionary<string, AgentBelief> _beliefs;
    private HashSet<AgentAction> _actions;
    private HashSet<AgentGoal> _goals;


    public Dictionary<string, AgentBelief> Beliefs => _beliefs;
    public HashSet<AgentAction> Actions => _actions; 
    public HashSet<AgentGoal> Goals => _goals;

    public Transform Transform => gameObject.transform; 

    [Inject] GoapFactory gFactory; // Doesn't work!
    IGoapPlanner gPlanner;
    
    void Awake() {
        navMeshAgent = GetComponent<NavMeshAgent>();
        animations = GetComponent<AnimationController>();
        rb = GetComponent<Rigidbody>();
        rb.freezeRotation = true;
        
        //gPlanner = gFactory.CreatePlanner(); // Does not work!
        gPlanner = new GoapPlanner();

        _beliefs = new Dictionary<string, AgentBelief>();
        _actions = new HashSet<AgentAction>(); 
        _goals = new HashSet<AgentGoal>();
    }
    void Update() {
        //animations.SetSpeed(navMeshAgent.velocity.magnitude);

        // Update the plan and current action if there is one
        UpdatePlan();
    }

    bool InRangeOf(Vector3 pos, float range) => Vector3.Distance(transform.position, pos) < range;
    
    public void ForceReplanning() {
        Debug.Log("Target changed, clearing current action and goal");
        // Force the planner to re-evaluate the plan
        currentAction = null;
        currentGoal = null;
    }

    void UpdatePlan()
    {
        if (currentAction == null) {
            Debug.Log("Calculating any potential new plan");
            CalculatePlan();

            if (actionPlan != null && actionPlan.Actions.Count > 0) {
                navMeshAgent.ResetPath();

                currentGoal = actionPlan.AgentGoal;
                Debug.Log($"Goal: {currentGoal.Name} with {actionPlan.Actions.Count} _ in plan");
                currentAction = actionPlan.Actions.Pop();
                Debug.Log($"Popped action: {currentAction.Name}");
                // Verify all precondition effects are true
                if (currentAction.Preconditions.All(b => b.Evaluate())) {
                    currentAction.Start();
                } else {
                    Debug.Log("Preconditions not met, clearing current action and goal");
                    currentAction = null;
                    currentGoal = null;
                }
            }
        }

        // If we have a current action, execute it
        if (actionPlan != null && currentAction != null) {
            currentAction.Update(Time.deltaTime);

            if (currentAction.Complete) {
                Debug.Log($"{currentAction.Name} complete");
                currentAction.Stop();
                currentAction = null;

                if (actionPlan.Actions.Count == 0) {
                    Debug.Log("Plan complete");
                    lastGoal = currentGoal;
                    currentGoal = null;
                }
            }
        }
    }
    void CalculatePlan() {
        var priorityLevel = currentGoal?.Priority ?? 0;
        
        HashSet<AgentGoal> goalsToCheck = _goals;
        
        // If we have a current goal, we only want to check _ with higher priority
        if (currentGoal != null) {
            Debug.Log("Current goal exists, checking _ with higher priority");
            goalsToCheck = new HashSet<AgentGoal>(_goals.Where(g => g.Priority > priorityLevel));
        }
        
        var potentialPlan = gPlanner.Plan(this, goalsToCheck, lastGoal);
        if (potentialPlan != null) {
            actionPlan = potentialPlan;
        }
    }
}