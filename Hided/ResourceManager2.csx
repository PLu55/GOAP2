using System.Collections;
using System.Collections.Generic;
using UnityEditorInternal;
using UnityEngine;
using PLu.Resources;
using System;

namespace PLu.Resources2
{
    [Serializable]
    public readonly struct ResourceKey : IEquatable<ResourceKey>
    {
        readonly Type type;
        readonly int hashedKey;

        public ResourceKey(Type type)
        {
            this.type = type;
            hashedKey = type.Name.ComputeFNV1aHash();
        }
        
        public bool Equals(ResourceKey other) => hashedKey == other.hashedKey;
        
        public override bool Equals(object obj) => obj is ResourceKey other && Equals(other);
        public override int GetHashCode() => hashedKey;
        public override string ToString() => type.Name;
        
        public static bool operator ==(ResourceKey lhs, ResourceKey rhs) => lhs.hashedKey == rhs.hashedKey;
        public static bool operator !=(ResourceKey lhs, ResourceKey rhs) => !(lhs == rhs);
    }

    [Serializable]
    public class ResourceEntry<T>
    {
        public ResourceKey TypeName { get; }
        public T Value { get; }
        public Type ValueType { get; }

        public ResourceEntry(ResourceKey typeName, T value)
        {
            TypeName = typeName;
            Value = value;
            ValueType = typeof(T);
            typeof(List<T>)
        }
        
        public override bool Equals(object obj) => obj is ResourceEntry<T> other && other.TypeName == TypeName;
        public override int GetHashCode() => TypeName.GetHashCode();
    }

    [Serializable]
    public class ResourceManager : MonoBehaviour
    {
        private Dictionary<Type, ResourceKey> _keyRegistry;
        private Dictionary<ResourceKey, object> _entries;

        void Awake()
        {
            _keyRegistry = new Dictionary<Type, ResourceKey>();
            _entries = new Dictionary<ResourceKey, object>();
            FindAllResources();        
        }
        private string TypeName<T>()
        {
            Type type = typeof(T);
            return type.Name;
        }
        private ResourceKey GetResourceKey<T>()
        {
            Type type = typeof(T);
            if (!_keyRegistry.TryGetValue(type, out var key))
            {
                key = new ResourceKey(type);
                _keyRegistry[type] = key;
            }
            return key;
        }
        public void Add<T>(T value)
        {
            ResourceKey key = GetResourceKey<T>();

            if (!_entries.TryGetValue(key, out var entry))
            {
                entry = new List<T>();
                _entries[key] = entry;
                _entries.Add(key, new List<T>());
            }
            ((List<T>)_entries[key]).Add(value);
        }
        public bool TryGetValues<T>(ResourceKey key, out List<T> values)
        {
            if (_entries.TryGetValue(key, out var entry))
            {
                values = (List<T>)entry;
                return true;
            }
            values = default;
            return false;
        }
        public void RegisterResourceType<T>()
        {
            Type type = typeof(T);
            if (!_keyRegistry.TryGetValue(type, out var key))
            {
                key = new ResourceKey(type);
                _keyRegistry[type] = key;
            }
        }
        private void FindAllResources()
        {
            GameObject[] gameObjects = GameObject.FindGameObjectsWithTag("Resource");
            foreach (var go in gameObjects)
            {
                foreach (var type in _keyRegistry.Keys)
                {
                    var component = go.GetComponent(type);
                    if (component != null)
                    {
                        Add(component);
                    }
                }
            }
        }

        public void DisplyContent() {
            foreach (var entry in _entries) {
                var entryType = entry.Value.GetType();

                if (entryType.IsGenericType && entryType.GetGenericTypeDefinition() == typeof(BlackboardEntry<>)) {
                    var valueProperty = entryType.GetProperty("Value");
                    if (valueProperty == null) continue;
                    var value = valueProperty.GetValue(entry.Value);
                    UnityEngine.Debug.Log($"Key: {entry.Key}, Value: {value}");
                }
            }
        }
    }

    public class ResourceManagerXXX: MonoBehaviour
    {
        private static ResourceManagerXXX _instance;
        public static ResourceManagerXXX Instance => _instance;

        [SerializeField] private List<string> ResourceTypes;
        private Dictionary<string, List<ResourceSource>> _resourceSources;
        private Dictionary<string, List<CraftingStation>> _craftingSations;
        
        private void Awake()
        {
            if (_instance == null)
            {
                _instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
            _resourceSources = new Dictionary<string, List<ResourceSource>>();
            _craftingSations = new Dictionary<string, List<CraftingStation>>();
            FindAllResources();        
        }




        void Start()
        {
            //_resources = new Dictionary<string, List<GameObject>>();
            //FindAllResouirces();
        }
        private void FindAllResources()
        {
            GameObject[] gameObjects = GameObject.FindGameObjectsWithTag("Resource");
            Debug.LogFormat("ResourceManager found {0} objects with the Resource tag", gameObjects.Length);
            foreach (GameObject gameObject in gameObjects)
            {
                var resourceSource = gameObject.GetComponent<ResourceSource>();
                if (resourceSource != null)
                {
                    Debug.LogFormat("ResourceManager found: {0} {1}", resourceSource, resourceSource.Name);
                    AddResource(resourceSource);
                    resourceSource.ResourceSourceExhausted += OnResourceExhausted;
                    continue;
                }
                if (gameObject.GetComponent<CraftingStation>() != null)
                {
                    var craftingStation = gameObject.GetComponent<CraftingStation>();
                    Debug.LogFormat("ResourceManager found: {0} {1}", craftingStation, craftingStation.Name);
                    AddCraftingStation(craftingStation);
                }
            }
        }
        public void AddCraftingStation(CraftingStation craftingStation)
        {
            if (!_craftingSations.ContainsKey(craftingStation.Name))
            {
                _craftingSations.Add(craftingStation.Name, new List<CraftingStation>());
            }
            _craftingSations[craftingStation.Name].Add(craftingStation);
        }
        public List<CraftingStation> GetCraftingStations(string type)
        {
            Debug.Assert(_craftingSations != null);
            if (_craftingSations.ContainsKey(type))
            {
                return _craftingSations[type];
            }
            Debug.LogFormat("No: {0} found!", type);
            return null;
        }
        public CraftingStation GetClosestCraftingStation(string type, Vector3 position)
        {
            List<CraftingStation> craftingStations = GetCraftingStations(type);
            if (craftingStations == null || craftingStations.Count == 0)
            {
                return null;
            }

            CraftingStation closest = null;
            float minDistance = float.MaxValue;

            foreach (var craftingStation in craftingStations)
            {
                Vector3 craftingStationPosition = craftingStation.GetComponent<Transform>().position;
                float sqrDistance = (position - craftingStationPosition).sqrMagnitude;
                if (sqrDistance < minDistance)
                {
                    closest = craftingStation;
                    minDistance = sqrDistance;
                }
            }
            return closest;
        }
        public void AddResource(ResourceSource resourceSource)
        {
            if (! _resourceSources.ContainsKey(resourceSource.Name))
            {
                _resourceSources.Add(resourceSource.Name, new List<ResourceSource>());
            }
            _resourceSources[resourceSource.Name].Add(resourceSource); 
        }
        public List<ResourceSource> GetResources(string type)
        {
            Debug.Assert(_resourceSources != null);
            if (_resourceSources.ContainsKey(type))
            {
                return _resourceSources[type];
            }
            Debug.LogFormat("No: {0} found!", type);
            return null;
        }
        public ResourceSource GetClosestResourceSource(string type, Vector3 position)
        {
            List<ResourceSource> resourceSources = GetResources(type);
            if (resourceSources == null || resourceSources.Count == 0)
            {
                return null;
            }

            ResourceSource closest = null;
            float minDistance = float.MaxValue;

            foreach (var resourceSource in resourceSources)
            {
                Vector3 resourceSourcePosition = resourceSource.GetComponent<Transform>().position;
                float sqrDistance = (position - resourceSourcePosition).sqrMagnitude;
                if (sqrDistance < minDistance)
                {
                    closest = resourceSource;
                    minDistance = sqrDistance;
                }
            }
            return closest;
        }
        private void OnResourceExhausted(ResourceSource resourceSource)
        {

            List<ResourceSource> resourceSources;
            if (_resourceSources.TryGetValue(resourceSource.Name, out resourceSources))
            {
                Debug.LogFormat("Removing {0} from ResourceManager", resourceSource);
                resourceSources.Remove(resourceSource);
            }
        }


    }
}
